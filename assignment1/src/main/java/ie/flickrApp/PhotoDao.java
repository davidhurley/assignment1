package ie.flickrApp;

import java.util.List;

import ie.flickrApp.entity.Photo;
public interface PhotoDao {
	     
	    public void saveOrUpdate(Photo photo);
	     
}
