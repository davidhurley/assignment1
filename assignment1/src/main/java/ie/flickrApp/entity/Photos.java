package ie.flickrApp.entity;

import java.util.List;
public class Photos {
	
	List<Photo> photo;
	public List<Photo> getPhoto() {
		return photo;
	}
	public void setPhoto(List<Photo> photo) {
		this.photo = photo;
	}
	
	@Override
    public String toString() {
        return "Value{" + '\'' + photo +  '}';
    }
	
}
