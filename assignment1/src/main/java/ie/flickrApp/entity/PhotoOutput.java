package ie.flickrApp.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoOutput {
	
	private Photos photos;	
	
	public Photos getPhotos() {
		return photos;
	}

	public void setPhotos(Photos photos) {
		this.photos = photos;
	}

	@Override
    public String toString() {
        return "Value{" +  "Page=" + photos +"";
    }

}