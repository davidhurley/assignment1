package ie.flickrApp.repository;

import org.springframework.web.client.RestTemplate;

import ie.flickrApp.entity.PhotoOutput;

public class GetImage {
	
	public PhotoOutput get(String result ,int amount) {	
		String api="https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=bc88f846f294150073f008defd11239f&tags="+result+"&per_page="+amount+"&page=1&format=json&nojsoncallback=1";
	    RestTemplate rest = new RestTemplate();
	    PhotoOutput photoOutput = rest.getForObject(api,PhotoOutput.class);
        
	   return photoOutput;
	}

}